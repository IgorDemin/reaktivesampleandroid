package com.example.reaktivesample.data

import kotlinx.coroutines.delay

/**
 * В таком репозитории можно инкапуслировать взаимодействие с API с action'ми, и нигде их не пробрасывать.
 * И id шники можно не пробрасывать.
 */
class AppealsRepository {
    private var lastId = 0L
    private val idToAppeal = HashMap<Long, Appeal>()

    suspend fun request(): Appeals {
        delay(3000)
        return Appeals()
    }

    inner class Appeals {
        val all = idToAppeal.entries.sortedBy { it.key }.map { it.value }

        suspend fun add() {
            delay(3000)
            val id = ++lastId
            idToAppeal[id] = Appeal(id, "Appeal $id", isActive = true)
        }
    }

    inner class Appeal(private val id: Long, val name: String, val isActive: Boolean) {
        suspend fun remove() {
            delay(3000)
            require(idToAppeal.remove(id) != null)
        }

        suspend fun archive() {
            delay(3000)
            idToAppeal[id] = Appeal(id, name, isActive = false)
        }

        suspend fun restore() {
            delay(3000)
            idToAppeal[id] = Appeal(id, name, isActive = true)
        }
    }
}