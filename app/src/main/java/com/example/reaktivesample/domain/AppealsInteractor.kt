package com.example.reaktivesample.domain

import com.dmi.util.scope.Scope
import com.dmi.util.scope.observable
import com.example.reaktivesample.data.AppealsRepository
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async

class AppealsInteractor(val scope: Scope, private val repository: AppealsRepository)  {
    var appeals: Deferred<Appeals> by observable(requestAsync())

    private fun refresh() {
        appeals.cancel()
        appeals = requestAsync()
    }

    private fun requestAsync() = scope.async(start = CoroutineStart.LAZY) {
        Appeals(repository.request())
    }

    inner class Appeals(private val repo: AppealsRepository.Appeals) {
        val active: List<Appeal>
        val archived: List<Appeal>

        init {
            val all = repo.all.map(::Appeal)
            active = all.filterNot(Appeal::isActive)
            archived = all.filter(Appeal::isActive)
        }

        suspend fun add() {
            repo.add()
            refresh()
        }
    }

    inner class Appeal(private val repo: AppealsRepository.Appeal) {
        val name: String get() = repo.name
        val isActive: Boolean get() = repo.isActive

        suspend fun remove() {
            repo.remove()
            refresh()
        }

        suspend fun archive() {
            repo.archive()
            refresh()
        }

        suspend fun restore() {
            repo.restore()
            refresh()
        }
    }
}