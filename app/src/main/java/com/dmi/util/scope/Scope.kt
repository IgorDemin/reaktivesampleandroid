package com.dmi.util.scope

import com.dmi.util.lang.ReadOnlyProperty2
import com.dmi.util.lang.ReadWriteProperty2
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

class Scope : Disposable, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job + CoroutineName(javaClass.simpleName)

    private val disposable = Disposables()
    private val job = Job()

    override fun dispose() {
        job.cancel()
        disposable.dispose()
    }

    fun <T : Disposable> disposable(value: T): T {
        disposable += value
        return value
    }

    fun <T> cachedAsync(
        start: CoroutineStart = CoroutineStart.DEFAULT,
        compute: suspend CoroutineScope.() -> T
    ) = cached {
        async(EmptyCoroutineContext, start, compute)
    }

    fun <T> observableDisposable(initial: T, dispose: (T) -> Unit = {}) =
        ObservableDisposableDelegate(observable(initial), dispose)

    fun <T : Disposable?> observableDisposable(initial: T) = observableDisposable(initial, dispose = { it?.dispose() })

    fun <T> cached(compute: () -> T) = CachedDelegate(compute, dispose = {})
    fun <T : Disposable?> cachedDisposable(compute: () -> T) = CachedDelegate(compute, dispose = { it?.dispose() })

    inner class ObservableDisposableDelegate<T>(
        observableDelegate: ReadWriteProperty2<Any, T>,
        private val dispose: (T) -> Unit
    ) : ReadWriteProperty2<Any?, T> {
        private var observable by observableDelegate

        init {
            disposable += object : Disposable {
                override fun dispose() = dispose(value)
            }
        }

        override var value: T
            get() = observable
            set (value) {
                dispose(observable)
                observable = value
            }
    }

    // todo throw exception on recursion
    inner class CachedDelegate<T>(
        private val compute: () -> T,
        private val dispose: (T) -> Unit
    ) : ReadOnlyProperty2<Any?, T> {
        private var observable by observable(Unit)
        private var cache: Cache? = null

        init {
            disposable += object : Disposable {
                override fun dispose() {
                    cache?.dispose()
                }
            }
        }

        override val value: T
            get() {
                if (cache == null) {
                    val (value, event) = onchange(compute)
                    val subscription = event.subscribe {
                        cache?.dispose()
                        cache = null
                        observable = Unit
                    }
                    cache = Cache(value, subscription)
                }
                observable // just call for intercept
                return cache!!.value
            }

        inner class Cache(val value: T, private val subscription: Disposable) : Disposable {
            override fun dispose() {
                dispose(value)
                subscription.dispose()
            }
        }
    }
}