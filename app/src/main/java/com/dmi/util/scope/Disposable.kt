package com.dmi.util.scope

interface Disposable {
    fun dispose()
}

class Disposables : Disposable {
    private val list = ArrayList<Disposable>()
    private var isDisposed = false

    operator fun plusAssign(disposable: Disposable) {
        check(!isDisposed)
        list.add(disposable)
    }

    override fun dispose() {
        check(!isDisposed)
        list.asReversed().forEach(Disposable::dispose)
        isDisposed = true
    }
}
